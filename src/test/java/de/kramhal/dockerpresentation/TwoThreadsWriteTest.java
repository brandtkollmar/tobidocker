package de.kramhal.dockerpresentation;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.io.LineReader;

public class TwoThreadsWriteTest {

	private static final String VALUE1 = "1";
	private static final String VALUE2 = "2";

	private File file;

	@Before
	public void setUp() throws IOException {
		file = File.createTempFile("unittest", ".txt");
	}

	@After
	public void delete() {
		if (file.exists())
			file.delete();
	}

	@Test
	public void shouldWriteAndReadFromTwoThreads() throws InterruptedException, IOException {

		Thread thread1 = new MyThread(file);
		Thread thread2 = new MyThread(file);

		thread1.start();
		Thread.sleep(500);
		thread2.start();
		Thread.sleep(1600);

		LineReader lineReader = new LineReader(new FileReader(file));
		assertEquals(VALUE1 + VALUE1 + VALUE2 + VALUE2, lineReader.readLine());
	}

	public class MyThread extends Thread {

		private File file;
		private DataAccessor dataAccessor;

		public MyThread(File file) {
			this.file = file;
		}

		@Override
		public void run() {
			try {
				dataAccessor = new DataAccessor(file);

				dataAccessor.addData(VALUE1);
				Thread.sleep(1000);
				dataAccessor.addData(VALUE2);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	};

}
