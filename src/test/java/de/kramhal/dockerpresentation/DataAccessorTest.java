package de.kramhal.dockerpresentation;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

public class DataAccessorTest {

	private static final String DATA = "Yo!";

	private DataAccessor sut;

	@Before
	public void setUp() throws IOException {
		File testFile = File.createTempFile("unittest", ".txt");
		System.out.println(testFile.getAbsolutePath());
		sut = new DataAccessor(testFile);
	}

	@Test
	public void testIt() throws IOException {
		sut.addData(DATA);

		String currentData = sut.getData();

		assertEquals(DATA, currentData);
	}

	@Test
	public void testIt2() throws IOException {
		sut.addData(DATA);
		sut.addData(DATA);

		String currentData = sut.getData();

		assertEquals(DATA + DATA, currentData);
	}
}
