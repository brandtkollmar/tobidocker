package de.kramhal.dockerpresentation;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.IOException;

import org.junit.ClassRule;
import org.junit.Test;

import io.dropwizard.testing.junit.ResourceTestRule;

public class DataResourceTest {

	private static final DataAccessor dataAccessor = mock(DataAccessor.class);

	@ClassRule
	public static final ResourceTestRule resources = ResourceTestRule.builder()
			.addResource(new DataResource(dataAccessor)).build();

	@Test
	public void testGet() throws IOException {
		resources.client().target("/data").request().get();
		verify(dataAccessor).getData();
	}

	@Test
	public void testPost() throws IOException {
		resources.client().target("/data").request().post(null);
		verify(dataAccessor).addData(DataResource.DATA);
	}

}
