(function() {

	var app = angular.module('docker-demo', [ 'ngResource' ]);

	app.factory('dockerService', function($resource) {
		return $resource('/docker/data');
	});

	app.controller('DockerController', function($scope, $interval,
			dockerService) {

		$scope.incCounter = function() {
			dockerService.save();
		};

		$scope.getCounter = function() {
			dockerService.get().$promise.then(function(httpResponse) {
				$scope.counter = httpResponse.content;
			});
		};

		$interval(function() {
			$scope.getCounter();
		}, 1000);
	});

})();