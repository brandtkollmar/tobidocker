package de.kramhal.dockerpresentation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class DockerPresentation extends Application<DockerPresentationConfiguration> {

	private static final Logger LOG = LoggerFactory.getLogger(DockerPresentation.class);

	public static void main(String[] args) throws Exception {
		new DockerPresentation().run(args);
	}

	@Override
	public void initialize(Bootstrap<DockerPresentationConfiguration> bootstrap) {
		super.initialize(bootstrap);
		bootstrap.addBundle(new AssetsBundle("/assets/", "/", "index.html"));
	}

	@Override
	public void run(DockerPresentationConfiguration configuration, Environment environment) throws Exception {

		LOG.debug("Started");

		DataAccessor dataAccessor = new DataAccessor(configuration.getFilePath());
		environment.jersey().register(new DataResource(dataAccessor));

	}

}
