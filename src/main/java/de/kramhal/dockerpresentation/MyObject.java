package de.kramhal.dockerpresentation;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MyObject {

	private String content;

	public MyObject() {
		// Jackson deserialization
	}

	public MyObject(String content) {
		this.content = content;
	}

	@JsonProperty
	public String getContent() {
		return content;
	}
}
