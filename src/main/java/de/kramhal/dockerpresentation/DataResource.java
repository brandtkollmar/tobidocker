package de.kramhal.dockerpresentation;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("data")
public class DataResource {

	private static Logger LOG = LoggerFactory.getLogger(DataResource.class);

	protected static final String DATA = "[…]";
	private DataAccessor dataAccessor;

	public DataResource(DataAccessor dataAccessor) {
		this.dataAccessor = dataAccessor;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public MyObject showData() throws IOException {
		LOG.debug("getting Data");
		return new MyObject(dataAccessor.getData());
	}

	@POST
	public void addData() throws IOException {
		LOG.debug("sending Data");
		dataAccessor.addData(DATA);
	}

}
