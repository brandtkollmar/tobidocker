package de.kramhal.dockerpresentation;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.LineReader;

public class DataAccessor {

	private static Logger LOG = LoggerFactory.getLogger(DataAccessor.class);

	private final File file;

	public DataAccessor(String filePath) throws IOException {
		this(new File(filePath));
	}

	protected DataAccessor(File file) throws IOException {
		if (!file.exists())
			file.createNewFile();
		this.file = file;
	}

	public String getData() throws IOException {
		LineReader lineReader = new LineReader(new FileReader(file));
		return lineReader.readLine();
	}

	public void addData(String data) throws IOException {
		
		FileWriter fileWriter = null;
		try {

			fileWriter = new FileWriter(file, true);
			fileWriter.append(data);
			fileWriter.flush();
		} catch (IOException e) {
			LOG.error("Error writing data!", e);
			e.printStackTrace();
			throw e;
		} finally {

			if (fileWriter != null)
				fileWriter.close();
		}
	}
	
}
