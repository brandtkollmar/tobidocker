package de.kramhal.dockerpresentation;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.dropwizard.Configuration;

public class DockerPresentationConfiguration extends Configuration{

	@NotEmpty
    private String filePath;

    @JsonProperty
    public String getFilePath() {
		return filePath;
	}

    @JsonProperty
    public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
}
